# Slasher
A browser extension for editing URLs

![Screenshot 1](https://github.com/bsawyer/slasher/raw/master/img/screenshot_1280x800_1.png)

## Features

- Break URL into easy to read parts
- Create and edit custom tokens
- Auto complete with custom tokens
- Keyboard support for all actions
- JSON import and export of custom tokens
- Supports light and dark mode
