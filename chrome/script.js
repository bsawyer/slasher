document.addEventListener('DOMContentLoaded', ()=>{
  getCurrentTab(tab => {
    slasher({
      url: tab && tab.url !== 'chrome://newtab/' ? tab.url : 'https://',
      target: document.querySelector('.slasher'),
      onGo: (url)=>{
        getCurrentTab(tab => {
          if(tab){
            chrome.tabs.update(tab.id, {url});
            window.close();
          }
        });
      }
    });
  });
});

function getCurrentTab(next){
	chrome.tabs.query({
		active: true,
		lastFocusedWindow: true
	}, function(tabs) {
		next(tabs[0]);
	});
}
