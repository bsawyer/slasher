window.slasher = function({
  onGo,
  url,
  target,
  disableScroll
}={}){
  init(url, onGo, target, disableScroll);
};

const config = {
  host: {
    delimiters: ['.']
  },
  path: {
    delimiters: ['/']
  },
  query: {
    delimiters: ['&', '=']
  }
}

const template = `<div class="container slasher">
  <div class="row">
    <div class="label">URL</div>
    <input class="input editor url" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
  </div>
  <div class="row">
    <div class="label">Host</div>
    <div class="input editor host" data-type="host" contenteditable="true" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div>
  </div>
  <div class="row">
    <div class="label">Path</div>
    <div class="input editor path" data-type="path" contenteditable="true" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div>
  </div>
  <div class="row">
    <div class="label">Query</div>
    <div class="input editor query" data-type="query" contenteditable="true" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></div>
  </div>
  <div class="row">
    <div class="col">
      <button class="token keybindings">Keybindings</button>
    </div>
    <div class="col import-export">
      <button class="token import">Import</button>
      <button class="token export">Export</button>
    </div>
  </div>
  <div class="keys">
    <table>
      <thead>
        <tr>
          <th>Keystroke</th>
          <th>Context</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="pre">enter</td>
          <td>editor</td>
          <td>Redirect current tab to URL</td>
        </tr>
        <tr>
          <td class="pre">space</td>
          <td>token</td>
          <td>Create/ edit custom token</td>
        </tr>
        <tr>
          <td class="pre">escape</td>
          <td>custom token editor</td>
          <td>Cancel and close custom token editor</td>
        </tr>
        <tr>
          <td class="pre">enter</td>
          <td>custom token editor</td>
          <td>Save and close custom token editor</td>
        </tr>
        <tr>
          <td class="pre">period</td>
          <td>host editor</td>
          <td>Insert new token</td>
        </tr>
        <tr>
          <td class="pre">forward slash</td>
          <td>path editor</td>
          <td>Insert new token</td>
        </tr>
        <tr>
          <td class="pre">equal|ampersand</td>
          <td>query editor</td>
          <td>Insert new token</td>
        </tr>
        <tr>
          <td class="pre">up|down arrow</td>
          <td>auto suggestion</td>
          <td>Change selected suggestion</td>
        </tr>
        <tr>
          <td class="pre">enter</td>
          <td>auto suggestion</td>
          <td>Insert selected suggestion</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>`;

function init(url, onGo, target, disableScroll){
  const state = getState();
  state.disableScroll = !!disableScroll;
  createContainer(target);
  Object.defineProperty(
    state,
    'onGo',
    {
      enumerable:false,
      value: onGo
    }
  );
  Object.defineProperty(
    state,
    'offcanvas',
    {
      enumerable:false,
      value: null,
      writable: true
    }
  );
  Object.defineProperty(
    state,
    'contentEditableElements',
    {
      enumerable:false,
      value: Array.prototype.slice.call(document.querySelectorAll('[contenteditable=true]'))
    }
  );
  Object.defineProperty(
    state,
    'urlElement',
    {
      enumerable:false,
      value: document.querySelector('.url')
    }
  );
  const keys = document.querySelector('.keys');
  document.querySelector('.keybindings').addEventListener('click', evt => keys.classList.toggle('show'))
  state.contentEditableElements.forEach(elm => createEditor(elm, state));
  state.urlElement.addEventListener('keydown', evt => {
    if(evt.keyCode === 13){
      evt.preventDefault();
      state.onGo && state.onGo(state.urlElement.value);
    }
  });
  state.urlElement.addEventListener('input', evt => {
    urlOnChange(state.urlElement, state, evt);
  });
  state.urlElement.value = url || '';
  urlOnChange(state.urlElement, state);
  initImportExport(state);
  window.addEventListener('keydown', evt => {
    if(evt.keyCode === 27 && keys.classList.contains('show')){
      evt.preventDefault();
      keys.classList.remove('show');
    }
  })
}

function createContainer(target){
  const div = document.createElement('div');
  div.innerHTML = template;
  if(target){
    target.parentNode.insertBefore(div.childNodes[0], target);
    target.remove();
    return;
  }
  document.body.appendChild(div.childNodes[0]);
}

function initImportExport(state){
  const fileInput = document.createElement('input');
  fileInput.setAttribute('style', 'display:none;');
  fileInput.setAttribute('type', 'file');

  fileInput.addEventListener('change', evt=> {
    const file = evt.target.files[0];
    let data;
    if(file){
      const reader = new FileReader();
      reader.onload = (e)=>{
        try {
          data = JSON.parse(e.target.result);
        } catch (err) {
          alert('Error parsing file');
          return;
        }
        state.tokens = data;
        saveState(state);
        urlOnChange(state.urlElement, state);
      };
      reader.readAsText(file);
    }
  });

  const imp = document.querySelector('.import');
  const exp = document.querySelector('.export');

  imp.addEventListener('click', evt => {
    fileInput.click();
  });

  exp.addEventListener('click', evt => {
    download(state);
  });

  document.body.appendChild(fileInput);
}

function download(state) {
    var a = document.createElement("a");
    var file = new Blob([JSON.stringify(state.tokens, null, 2)], {type: 'application/json'});
    a.href = URL.createObjectURL(file);
    a.download = 'slasher-tokens.json';
    a.click();
}

function createEditor(element, state){
  const type = getEditorType(element);
  const delimiters = config[type].delimiters;
  element.addEventListener('keydown', evt => {
    if(evt.keyCode === 13){
      evt.preventDefault();
      state.onGo && state.onGo(state.urlElement.value);
    }
  });
  element.addEventListener('input', evt => {
    onInput(element, state, evt);
  });
  element.addEventListener('focus', evt => {
    focusLastTokenInEditor(element, state);
  });
  const observer = new MutationObserver(() => {
    if(element.childNodes.length){
      element.setAttribute('tabindex', '-1')
    }else{
      element.removeAttribute('tabindex');
    }
  });
  observer.observe(element, {
    attributes: false,
    childList: true,
    subtree: true
  });
}

function focusLastTokenInEditor(element, state){
  let t = getLastToken(element);
  setFocusAtEnd(t);
  !state.disableScroll && t.scrollIntoView(false);
}

function getInactiveTokens(element){
  return Array.prototype.slice.call(element.childNodes).filter(elm => isTokenWrap(elm) && !elm.childNodes[0].getAttribute('data-token-value'));
}

function getActiveTokens(element){
  return Array.prototype.slice.call(element.childNodes).filter(elm => isTokenWrap(elm) && elm.childNodes[0].getAttribute('data-token-value'));
}

function getLastToken(element){
  if(element.childNodes && element.childNodes.length){
    const child = element.childNodes[element.childNodes.length - 1]
    if(child && isToken(child.childNodes[0])){
      return child.childNodes[0];
    }
  }
  return element;
}

function isToken(elm){
  return elm && elm.getAttribute('data-type') === 'token';
}

function isTokenWrap(elm){
  return elm && elm.getAttribute('data-type') === 'token-wrap' && isToken(elm.childNodes[0]);
}

function createToken(text, element, state){
  const wrapper = document.createElement('span');
  const span = document.createElement('span');
  const type = getEditorType(element);

  wrapper.setAttribute('contenteditable', 'false');
  wrapper.setAttribute('data-type', 'token-wrap');
  wrapper.classList.add('token-wrap');

  span.classList.add('token');
  span.setAttribute('data-type', 'token');
  span.setAttribute('contenteditable', 'true');
  span.textContent = text || '';

  // token events
  span.addEventListener('focus', (evt)=>{
    evt.preventDefault();
    evt.stopPropagation();
    setFocusAtEnd(span);
    !state.disableScroll && span.scrollIntoView(false);
    debounceSuggestions(element, state);
  });

  wrapper.addEventListener('keydown', evt => {
    // space
    if(evt.keyCode === 32){
      evt.preventDefault();
      evt.stopPropagation();
      showEdit(element, wrapper, state, evt);
    }
    // 38 up, 40 down, enter is 13
    if(state.offcanvas && state.offcanvas.type === 'suggestion' && (evt.keyCode === 38 || evt.keyCode === 40 || evt.keyCode === 13)){
      evt.preventDefault();
      evt.stopPropagation();
      updateSuggestions(element, wrapper, state, evt);
    }
    // escape 27
    // if(evt.keyCode === 27 && state.offcanvas && state.offcanvas.type){
    //   removeOffcanvas(state);
    // }
  });

  span.addEventListener('blur', evt => {
    if(state.offcanvas && state.offcanvas.type === 'suggestion'){
      removeOffcanvas(state);
    }
    if(span.textContent === ''){
      if(wrapper.previousSibling && isTokenWrap(wrapper.previousSibling)){
        const prev = wrapper.previousSibling.childNodes[0];
        setTimeout(e => setFocusAtEnd(prev));
      }else{
        focusLastTokenInEditor(element, state);
      }
      setTimeout(()=>{
        document.contains(wrapper) && wrapper.remove();
      });
    }
  });

  wrapper.appendChild(span);
  return wrapper;
}

function createEditOptions(element){
  const div = document.createElement('div');
  const type = getEditorType(element);
  const input = document.createElement('input');

  input.classList.add('input');
  input.classList.add('options-input');
  input.setAttribute('placeholder', 'Create token');

  div.classList.add('offcanvas');
  div.classList.add('offcanvas-row');
  div.classList.add('offcanvas-row--active');
  div.classList.add(`offcanvas-type-${type}`);

  let editorType;
  // if(type === 'query'){
  //   editorType = createQueryEditorToggle()
  // }
  editorType = document.createElement('div');
  editorType.classList.add(`offcanvas-label`);
  editorType.textContent = type.substring(0,1);

  div.appendChild(editorType);
  div.appendChild(input);
  return div;
}

const verticalOffset = 3;

const inputAttrs = {
  type:"text",
  autocomplete:"off",
  autocorrect:"off",
  autocapitalize:"off",
  spellcheck:"false"
};

function showEdit(element, wrapper, state, evt){
  const options = createEditOptions(element);
  const offset = wrapper.getBoundingClientRect();
  const type = getEditorType(element);

  document.body.appendChild(options);

  removeOffcanvas(state);

  wrapper.childNodes[0].classList.add('token--focus');
  options.setAttribute('style', getOffcanvasStyle(wrapper, options, state));

  const input = options.querySelector('input');
  Object.keys(inputAttrs).forEach(k => input.setAttribute(k, inputAttrs[k]));
  const tokenValue = wrapper.childNodes[0].getAttribute('data-token-value');
  if(tokenValue){
    input.value = tokenValue;
    input.setAttribute('placeholder', 'Edit token');
  }

  input.addEventListener('keydown', evt => {
    // escape
    if(evt.keyCode === 27){
      evt.preventDefault();
      setFocusAtEnd(state.offcanvas.wrapper.childNodes[0]);
    }
    // enter
    if(evt.keyCode === 13){
      evt.preventDefault();
      if(input.value){
        saveToken(state);
        if(wrapper.childNodes[0].getAttribute('data-token-value')){
          getActiveTokens(element)
            .filter(elm => elm.textContent === wrapper.textContent && elm !== wrapper)
            .forEach(elm => elm.childNodes[0].setAttribute('data-token-value', getTokenValue(type, wrapper.textContent, state)))
        }
        replaceToken(type, wrapper, state);
        updateUrl(state);
        setFocusAtEnd(state.offcanvas.wrapper.childNodes[0]);
      }
    }
  });
  input.addEventListener('blur', evt => removeOffcanvas(state))
  input.focus();
  state.offcanvas = {
    editor: element,
    type: 'edit',
    edit: options,
    wrapper
  };
}

function removeOffcanvas(state){
  if(!state.offcanvas) return;
  if(state.offcanvas.type === 'edit'){
    state.offcanvas.edit.remove();
  }else if(state.offcanvas.type === 'suggestion'){
    state.offcanvas.suggestions.remove();
  }
  state.offcanvas.wrapper && state.offcanvas.wrapper.childNodes[0].classList.remove('token--focus');
  state.offcanvas = null;
}

function saveToken(state){
  const type = getEditorType(state.offcanvas.editor);
  let tokenName, tokenValue;
  if(state.offcanvas.wrapper.childNodes[0].getAttribute('data-token-value')){
    // we're actually editing the toke value
    tokenName = state.offcanvas.wrapper.textContent;
    tokenValue = state.offcanvas.edit.querySelector('input').value;
  }else{
    // editing a new token name
    tokenName = state.offcanvas.edit.querySelector('input').value;
    tokenValue = state.offcanvas.wrapper.textContent;
  }

  if(!state.tokens){
    state.tokens = {};
  }
  if(!state.tokens[type]){
    state.tokens[type] = {};
  }
  state.tokens[type][tokenName] = tokenValue;
  saveState(state);
}

function replaceToken(type, element, state){
  let tokenName, tokenValue;
  if(!element.childNodes[0].getAttribute('data-token-value')){
    // check names
    tokenName = getTokenName(type, element.textContent, state);
    tokenValue = element.textContent;
  }else{
    // check values
    tokenName = element.textContent;
    tokenValue = getTokenValue(type, element.textContent, state);
  }

  if(tokenName && tokenValue){
    setTokenAsActive(tokenName, tokenValue, element);
  }
}

function isTokenValueLiteral(type, value){
  return config[type].delimiters.some(d => value.indexOf(d) !== -1);
}

function setTokenAsActive(name, value, element){
  const type = getEditorType(element.parentNode);
  if(isTokenValueLiteral(type, value)){
    element.childNodes[0].setAttribute('data-token-literal', 'true');
  }
  element.childNodes[0].setAttribute('data-token-value', value);
  element.childNodes[0].classList.add('token--active');
  element.childNodes[0].textContent = name;
}

function getTokenValue(type, name, state){
  if(!state.tokens || !state.tokens[type] || !state.tokens[type][name]){
    return null;
  }
  return state.tokens[type][name];
}

function getTokenName(type, value, state){
  if(!state.tokens || !state.tokens[type]){
    return null;
  }
  let name;
  Object.keys(state.tokens[type]).some(k => {
    if(state.tokens[type][k] === value){
      name = k;
      return true;
    }
  });
  return name;
}

function focusEditor(editor, state){
  clearEditorFocus(editor, state);
  editor.classList.add('editor--focus');
}

function clearEditorFocus(editor, state){
  state.contentEditableElements.some(elm => {
    if(elm.classList.contains('editor--focus') && elm !== editor){
      // fake focus, clear out any leftovers
      collapseInput(elm, state);
      elm.classList.remove('editor--focus');
      return true;
    }
  });
}

function onInput(element, state, evt){
  replaceTextWithToken(element, state, evt);
  collapseInput(element, state, evt);
  splitTokens(element, state, evt);

  if(!evt){
    // when the url is updated check for active tokens
    activateTokens(element, state);
  }

  if(evt){
    const type = getEditorType(element);
    getActiveTokens(element).forEach(elm => checkActive(type, elm, state));
    debounceSuggestions(element, state);
  }

  // tabbing out of editor into something other than editor breaks this
  // const cursorNode = getCursorNode();
  // if(evt && cursorNode && !isToken(cursorNode)){
  //   focusLastTokenInEditor(element);
  // }

  evt && updateUrl(state);
}

function activateTokens(element, state){
  const type = getEditorType(element);
  getInactiveTokens(element).forEach(elm => replaceToken(type, elm, state));
  getActiveTokens(element).forEach(elm => checkActive(type, elm, state));
}

function checkActive(type, element, state){
  const tokenName = getTokenName(type, element.childNodes[0].getAttribute('data-token-value'), state);
  if(tokenName !== element.textContent){
    element.childNodes[0].classList.remove('token--active');
    element.childNodes[0].removeAttribute('data-token-value');
    element.childNodes[0].removeAttribute('data-token-literal');
  }
}

function getTokenLiterals(type, state){
  const delimiters = config[type].delimiters;
  if(state.tokens && state.tokens[type]){
    return Object.keys(state.tokens[type])
      .filter(k => delimiters.some(d => state.tokens[type][k].indexOf(d) !== -1))
      .sort((a,b)=>{
        if(state.tokens[type][a].length > state.tokens[type][b].length){
          return -1;
        }
        if(state.tokens[type][b].length > state.tokens[type][a].length){
          return 1;
        }
        return 0;
      })
      .map(k => {
          return {
            name: k,
            value: state.tokens[type][k]
          }
      });
  }
}

function isTokenLiteral(element, wrapper, state){
  const type = getEditorType(element);
  const value = wrapper.textContent;
  if(state.tokens && state.tokens[type]){
    return getTokenLiterals(type, state).some(token => {
      const i = value.indexOf(token.value);
      let span;
      if(i !== -1){
        if(i > 0){
          // hope the previous character is a delimiter?
          span = createToken(value.substring(0, i - 1), element, state);
          element.insertBefore(span, wrapper);
        }
        span = createToken(token.name, element, state);
        span.childNodes[0].setAttribute('data-token-value', token.value);
        span.childNodes[0].setAttribute('data-token-literal', 'true');
        span.childNodes[0].classList.add('token--active');
        element.insertBefore(span, wrapper);
        if(i + token.value.length < value.length){
          // hope the previous character is a delimiter?
          span = createToken(value.substring(i + token.value.length + 1), element, state);
          element.insertBefore(span, wrapper);
        }
        wrapper.remove();
        return true;
      }
    });
  }
  return false;
}

function splitTokens(element, state, evt){
  const type = getEditorType(element);
  const delimiters = config[type].delimiters;

  if(type === 'query'){
    // do query funky dance
    splitQueryTokens(element, state, evt);
  }else{
    mapLiteralChildren(element, state);
    let children = Array.prototype.slice.call(element.childNodes);
    for(let i = 0; i < children.length; i++){
      let parts = children[i].textContent.split(delimiters[0]);
      const tokenValue = children[i].childNodes[0].getAttribute('data-token-value');
      const isLiteral = children[i].childNodes[0].getAttribute('data-token-literal');
      let tokenName;
      if(tokenValue){
        tokenName = getTokenName(type, tokenValue, state);
      }
      if(parts.length > 1){
        parts = parts.map(p => {
          const span = createToken(p, element, state);
          if(p === tokenName){
            // make active token
            span.childNodes[0].setAttribute('data-token-value', tokenValue);
            span.childNodes[0].classList.add('token--active');
            if(isLiteral === 'true'){
              span.childNodes[0].setAttribute('data-token-literal', 'true');
            }
          }
          element.insertBefore(span, children[i]);
          return span;
        });
        children[i].remove();
        evt && setFocusAtEnd(parts[parts.length - 1].childNodes[0]);
      }
    }
  }
}

function mapLiteralChildren(element, state){
  let children = Array.prototype.slice.call(element.childNodes);
  let index = 0;
  while(children.length){
    if(!isTokenLiteral(element, children[0], state)){
      index++;
    }
    children = Array.prototype.slice.call(element.childNodes, index);
  }
}

function splitQueryTokens(element, state, evt){
  mapLiteralChildren(element, state);
  let children = Array.prototype.slice.call(element.childNodes);
  for(let i = 0; i < children.length; i++){
    let pairs = children[i].textContent.split('&');
    const tokenValue = children[i].childNodes[0].getAttribute('data-token-value');
    const isLiteral = children[i].childNodes[0].getAttribute('data-token-literal');
    let tokenName;
    if(tokenValue){
      tokenName = getTokenName('query', tokenValue, state);
    }
    const pairType = children[i].getAttribute('data-pair-type') || 'key';
    pairs = pairs.reduce((prev, pair, reduceIndex) => {
      prev = prev.concat(pair.split('=').map((s, index) => {
        const span = createToken(s, element, state);
        if(s === tokenName){
          // make active token
          span.childNodes[0].setAttribute('data-token-value', tokenValue);
          span.childNodes[0].classList.add('token--active');
          if(isLiteral === 'true'){
            span.childNodes[0].setAttribute('data-token-literal', 'true');
          }
        }
        span.setAttribute('data-pair-type', index === 0 ? (reduceIndex === 0 ? pairType : 'key') : 'value');
        // element.insertBefore(span, children[i]);
        return span;
      }));
      return prev;
    }, []);
    if(pairs.length > 1){
      pairs.forEach(elm => element.insertBefore(elm, children[i]));
      children[i].remove();
      evt && setFocusAtEnd(pairs[pairs.length - 1].childNodes[0]);
    }
  }
}

function collapseInput(element, state, evt){
  let elm = [];
  const children = Array.prototype.slice.call(element.childNodes);
  const span = createToken('', element, state);
  for(let i = 0; i < children.length; i++){
    if(children[i].nodeType === Node.ELEMENT_NODE && (!isTokenWrap(children[i]) || children[i].textContent === '')){
      elm.push(children[i]);
    }else{
      if(elm.length){
        elm.reduce(reduceInput, span);
        elm = [];
      }
    }
  }
  if(elm.length){
    elm.reduce(reduceInput, span);
    if(span.textContent.length){
      element.appendChild(span);
      evt && setFocusAtEnd(span.childNodes[0]);
    }else{
      evt && setFocusAtEnd(element);
    }
  }
}

function reduceInput(prev, curr){
  prev.childNodes[0].textContent += curr.textContent;
  curr.remove();
  return prev;
}

function replaceTextWithToken(element, state, evt){
  for(let i = 0; i < element.childNodes.length; i++){
    let child = element.childNodes[i];
    if(child.nodeType === Node.TEXT_NODE){
      const span = createToken(child.textContent, element, state);
      element.insertBefore(span, child);
      child.remove();
      evt && setFocusAtEnd(span.childNodes[0]);
      break;
    }
  }
}

function getEditorType(element){
  return element.getAttribute('data-type');
}

function setFocusAtEnd(element){
  var range = document.createRange();
  range.selectNodeContents(element);
  range.collapse(false);
  var sel = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
}

function getCursorNode(){
  const range = window.getSelection().getRangeAt(0);
  return range.commonAncestorContainer.parentNode;
}

function urlOnChange(element, state, evt){
  const parsed = parseUrl(element.value);
  state.parsed = parsed;
  state.parsed.pathname = state.parsed.pathname.replace(/\/$/, '');

  clearEditors(state);
  populateEditors(state);
  formatEditors(state);
}

function getSpanTokenValue(element){
  return element.childNodes[0].getAttribute('data-token-value') || element.textContent;
}

function mapQueryLiteral(query, value){
  value = value.split('&');
  value.forEach(v => {
    v = v.split('=');
    if(query[v[0]]){
      query[v[0]].push(v[1]);
    }else{
      query[v[0]] = [v[1]];
    }
  });
}

function updateUrl(state){
  const parsed = parseUrl(state.urlElement.value);
  state.parsed = parsed;
  parsed.host = Array.prototype.slice.call(state.contentEditableElements[0].childNodes).map(elm => getSpanTokenValue(elm)).join('.');
  parsed.pathname = Array.prototype.slice.call(state.contentEditableElements[1].childNodes).map(elm => getSpanTokenValue(elm)).join('/');
  const query = {};
  let lastKey;
  Array.prototype.slice.call(state.contentEditableElements[2].childNodes).forEach((elm)=>{
    if(elm.childNodes[0].getAttribute('data-token-literal') === 'true'){
      mapQueryLiteral(query, getSpanTokenValue(elm));
      return;
    }
    if(elm.getAttribute('data-pair-type') === 'value'){
      query[lastKey].push(getSpanTokenValue(elm));
      return;
    }
    lastKey = getSpanTokenValue(elm);
    query[lastKey] = query[lastKey] || [];
  });
  parsed.search = Object.keys(query).map(k => query[k].map(v => k + '=' + v).join('&')).join('&');

  if(state.contentEditableElements[0].childNodes.length){
    let port = getSpanTokenValue(state.contentEditableElements[0].childNodes[state.contentEditableElements[0].childNodes.length -1]);
    if(port && port.indexOf(':') !== -1){
      parsed.port = parseFloat(port.split(':').pop());
    }
  }

  state.urlElement.value = parsed.getAttribute('href');
}

function parseUrl(url){
  const a = document.createElement('a');
  a.setAttribute('href', url);
  return a;
}

function loadUrl(state){
  // open new tab or chrome extension update tab url with state.urlElement.value
}

function clearEditors(state){
  state.contentEditableElements.forEach(elm => elm.innerHTML = '');
}

function populateEditors(state){
  if(state.parsed.host){
    state.contentEditableElements[0].textContent = state.parsed.host; //need to treat host string like it was input
  }
  if(state.parsed.pathname){
    state.contentEditableElements[1].textContent = state.parsed.pathname.replace('/',''); //need to treat host string like it was input
  }
  if(state.parsed.search){
    state.contentEditableElements[2].textContent = state.parsed.search.replace('?',''); //need to treat host string like it was input
  }
}

function formatEditors(state){
  state.contentEditableElements.forEach(elm => onInput(elm, state));
}

function createSuggestions(element, suggestions, state){
  const type = getEditorType(element);
  const div = document.createElement('div');

  // div.classList.add('offcanvas');
  // div.classList.add('suggestions');
  // div.classList.add(`offcanvas-type-${type}`);

  suggestions.forEach(s => {
    div.appendChild(createSuggestion(s, state))
  });

  return div;
}

function createSuggestion(suggestion, state){
  const div = document.createElement('div');
  const label = document.createElement('div');
  const option = document.createElement('div');

  div.classList.add(`suggestion`);
  div.classList.add(`offcanvas-row`);
  div.setAttribute('data-value', suggestion.value);
  div.setAttribute('data-name', suggestion.name);

  label.classList.add(`offcanvas-label`);

  option.classList.add('offcanvas-option');
  option.textContent = suggestion.name;

  div.appendChild(label);
  label.after(option);

  div.addEventListener('mousedown', evt => {
    evt.preventDefault();
    const rows = Array.prototype.slice.call(state.offcanvas.suggestions.querySelectorAll('.offcanvas-row'));
    rows.forEach(r => r.classList.remove('offcanvas-row--active'));
    div.classList.add('offcanvas-row--active');
    selectSuggestion(state);
  });

  return div;
}

function getOffcanvasStyle(element, offcanvas){
  const offset = element.getBoundingClientRect();

  const horizontalStyle = (offset.left + offcanvas.clientWidth + 30) < window.innerWidth ?
    `left:${offset.left < 0 ? 0 : offset.left}px;` :
    `left:${offset.left - (offcanvas.clientWidth - element.clientWidth)}px;`;

  const verticalStyle = (offset.top + window.pageYOffset + offcanvas.clientHeight + 30) < window.innerHeight ?
    `top:${offset.top + window.pageYOffset + offset.height + verticalOffset}px;` :
    `top:${offset.top + window.pageYOffset - offcanvas.clientHeight - verticalOffset}px;`;

  return horizontalStyle + verticalStyle;
}

function getSuggestionResults(type, query, state){
  const results = [];
  if(state.tokens && state.tokens[type]){
    Object.keys(state.tokens[type]).forEach((name)=>{
      const value = state.tokens[type][name];
      if(name.indexOf(query) !== -1 || value.indexOf(query) !== -1){
        results.push({
          name,
          value
        });
      }
    });
  }
  return results;
}

function showSuggestions(element, wrapper, state){
  const type = getEditorType(element);
  const suggestionResults = getSuggestionResults(type, wrapper.textContent, state);
  if(!suggestionResults.length){
    // remove if there is one
    removeOffcanvas(state);
    return;
  }
  let suggestions = createSuggestions(element, suggestionResults, state);
  if(state.offcanvas && state.offcanvas.type === 'edit'){
    removeOffcanvas(state);
  }
  if(state.offcanvas && state.offcanvas.suggestions){
    // suggestions = state.offcanvas.suggestions;
    // check suggestions
    // update style
    removeOffcanvas(state);
    document.body.appendChild(suggestions);
  }else{
    document.body.appendChild(suggestions);
  }

  suggestions.setAttribute('class', `offcanvas suggestions offcanvas-type-${type}`);
  Array.prototype.forEach.call(suggestions.querySelectorAll('.offcanvas-label'), elm => elm.textContent = type.substring(0,1));
  suggestions.querySelector('.offcanvas-row').classList.add('offcanvas-row--active');
  suggestions.setAttribute('style', getOffcanvasStyle(wrapper, suggestions, state));

  wrapper.childNodes[0].classList.add('token--focus');

  state.offcanvas = {
    editor: element,
    type: 'suggestion',
    suggestions,
    wrapper
  };
}

function selectSuggestion(state){
  const selectedSuggestion = state.offcanvas.suggestions.querySelector('.offcanvas-row--active');
  setTokenAsActive(selectedSuggestion.getAttribute('data-name'), selectedSuggestion.getAttribute('data-value'), state.offcanvas.wrapper);
  updateUrl(state);
  setFocusAtEnd(state.offcanvas.wrapper.childNodes[0]);
  removeOffcanvas(state);
}

function updateSuggestions(element, wrapper, state, evt){
  const type = getEditorType(element);
  const selectedSuggestion = state.offcanvas.suggestions.querySelector('.offcanvas-row--active');
  if(evt.keyCode === 13){
    selectSuggestion(state);
    return;
  }
  const dir = evt.keyCode === 38 ? -1 : 1;
  const rows = Array.prototype.slice.call(state.offcanvas.suggestions.querySelectorAll('.offcanvas-row'));
  const activeIndex = rows.indexOf(selectedSuggestion);
  const newIndex = activeIndex + dir >= 0 && activeIndex + dir <= rows.length - 1 ? activeIndex + dir : dir === -1 ? rows.length - 1 : 0;
  rows.forEach(r => r.classList.remove('offcanvas-row--active'));
  rows[newIndex].classList.add('offcanvas-row--active');
  !state.disableScroll && rows[newIndex].scrollIntoView(false);
}

function debounceSuggestions(element, state){
  if(state.offcanvas && state.offcanvas.timeout){
    return;
  }
  if(!state.offcanvas){
    state.offcanvas = {};
  }
  state.offcanvas.timeout = setTimeout(()=>{
    const cursorNode = getCursorNode();
    const wrap = isTokenWrap(cursorNode) ? cursorNode : cursorNode.parentNode;
    // weird timeout thing when cleared
    if(isTokenWrap(wrap) && wrap.textContent.length >= 1 && !wrap.childNodes[0].getAttribute('data-token-value')){
      showSuggestions(element, wrap, state);
    }
    if(state.offcanvas){
      state.offcanvas.timeout = null;
    }
  }, 200);
}

function saveState(state){
  try{
    state = JSON.stringify(state);
  }catch(e){
    state = JSON.stringify({});
  }
  localStorage.setItem('state', state);
}

function getState(){
  let state = localStorage.getItem('state');
  try{
    state = JSON.parse(state) || {};
  }catch(e){
    state = {};
  }
  return state;
}
